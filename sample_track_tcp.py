#!/usr/bin/env python3

import datetime
import os, time

def split_hex(data, n):
    return [data[i:i+n] for i in range(0, len(data), n)]

def convert_remote_network_byte(address):

    hex_addr, hex_port = address.split(':')

    addr_list = split_hex(hex_addr, 2)
    addr_list.reverse()

    addr = ".".join(map(lambda x: str(int(x, 16)), addr_list))
    port = str(int(hex_port, 16))

    return "{}:{}".format(addr, port)

def filter_remote_ip(filename):

    with open(filename) as fp:
        next(fp)
        result = []
        for line in fp:
            remote_address = convert_remote_network_byte(line.split(' ')[2])
            result.append(remote_address)
        
        return result


before = dict ([(f, None) for f in filter_remote_ip("sample_proc_net")])
while 1:
  time.sleep (10)
  after = dict ([(f, None) for f in filter_remote_ip("sample_proc_net")])
  added = [f for f in after if not f in before]
  removed = [f for f in before if not f in after]
  if added: print(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S") + ": New connection:", ", ".join (added))
  if removed: print(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S") + ": Removed connection:", ", ".join (removed))
  before = after
