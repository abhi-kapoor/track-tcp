# track-tcp

track-tcp is a python tool to track remote TCP connections. Can't use tcpdump? Don't worry, track-tcp has got your back 😀

## Usage

In order to use this, simply run the program **track_tcp.py** on the server on which you want to track TCP connections. A sample run for demonstation is included in this repo. 

## Sample Run

A sample run is included in this project which is triggered via the pipeline. This sample run is just a POC that the code works and gives the desired output. There is a sample file called 'sample_proc_net' which has a line removed and added back to it to test the code and output. The line is added and removed using content from file sample_proc_net_tcp. The program we run is sample_track_tcp.py which uses file sample_proc_net. 



